#@author Colin Cruse
#@since 4/30/14



def InsertionSort(a):
    
    b = list(a)
    for i in range(0,len(a)):
        val = b[i]
        j = i-1
        
        while(j>=0 and b[j] > val):
            
            b[j+1] = b[j]
            j=j-1
           
        b[j+1] = val
        
    return(b)

def MergeSort(a):
   
    
    if len(a)>1:
        mid = len(a)//2
        left = a[:mid]
        right = a[mid:]
        
        MergeSort(left)
        MergeSort(right)

        i=0
        j=0
        k=0
        
        while i<len(left) and j<len(right):
            if left[i]<right[j]:
                
                a[k]=left[i]
                i=i+1
                
            else:
                a[k]=right[j]
                j=j+1
                
            k=k+1
    
        while i<len(left):
            a[k]=left[i]
            i=i+1
            k=k+1

        while j<len(right):
            a[k]=right[j]
            j=j+1
            k=k+1
    return a

def QuickSort(a):
    
    a = list(a)
    if(len(a) < 2):
        
        return a
    else:
        pivot = a[0]
       
        l = QuickSort((x for x in a[1:] if x < pivot))#partioning using list comprehension  and Recursing

        g = QuickSort((x for x in a[1:] if x >=pivot))#partioning using list comprehension  and Recursing

    
        return l+[pivot]+g
    
    
        




